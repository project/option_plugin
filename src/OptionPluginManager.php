<?php

declare(strict_types=1);

namespace Drupal\option_plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\option_plugin\Contracts\OptionPluginInterface;
use Drupal\option_plugin\Contracts\OptionPluginManagerInterface;
use Drupal\option_plugin\Annotation\OptionPlugin;

/**
 * Define the attribute option manager.
 */
class OptionPluginManager extends DefaultPluginManager implements OptionPluginManagerInterface {

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Define the attribute option manager constructor.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/OptionPlugin',
      $namespaces,
      $module_handler,
      OptionPluginInterface::class,
      OptionPlugin::class
    );
    $this->alterInfo('option_plugin');
    $this->setCacheBackend($cache_backend, 'option_plugin');
  }

}
