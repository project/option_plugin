<?php

namespace Drupal\option_plugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;

/**
 * Defines a trait for option plugin contexts (i.e. layouts, paragraphs).
 */
trait OptionPluginContextTrait {

  /**
   * Returns an array of option defitions for the handler.
   *
   * @return array
   *   An array of definitions.
   */
  protected function getContextDefinition() {
    return $this->optionPluginConfigurationDiscovery()->getProcessedContextDefinition($this->getContextId(), $this->getPluginId());
  }

  /**
   * Get the options settings for this context.
   *
   * @return array
   *   The options settings.
   */
  protected function getContextOptions() {
    return $this->optionPluginConfigurationDiscovery()->getContextOptions($this->getContextId(), $this->getPluginId());
  }

  /**
   * Gets the form item for a single option.
   *
   * @param string $option_id
   *   The option id.
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param mixed $values
   *   The default value.
   *
   * @return array
   *   The form item.
   */
  protected function getOptionPluginForm(string $option_id, array $form, FormStateInterface $form_state, $values = []) {
    if ($option_definition = $this
      ->optionPluginConfigurationDiscovery()
      ->getOptionDefinition($option_id)) {

      $instance = $this->optionPluginManager()->createInstance(
        $option_definition['plugin'],
        $option_definition,
      );
      $instance->setValues($values);
      $subform = [];
      return $instance->buildConfigurationForm(
        $subform,
        SubformState::createForSubform($subform, $form, $form_state)
      );
    }
  }

  /**
   * Decorates a build array with the option value.
   *
   * @param string $option_id
   *   The option id.
   * @param string $value
   *   The option value.
   * @param array $build
   *   The build array to decorate.
   *
   * @return array
   *   The decorated build array.
   */
  protected function buildOptions(string $option_id, $values = [], array $build = []) {
    if ($instance = $this->getOptionPlugin($option_id)) {
      $instance->setValues($values);
      return $instance->build($build);
    }
  }

  /**
   * Submits an option plugin form and returns the processed values.
   *
   * @param string $option_id
   *   The option id.
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The values.
   */
  protected function submitOptionPluginForm(string $option_id, array $form, FormStateInterface $form_state) {
    $instance = $this->getOptionPlugin($option_id);
    $subform_state = SubformState::createForSubform($form[$option_id], $form, $form_state);
    $instance->submitConfigurationForm($form[$option_id], $subform_state);
    return $instance->getValues();
  }

  /**
   * Returns an option plugin instance.
   *
   * @param string $option_id
   *   The option plugin id.
   *
   * @return \Drupal\option_plugin\Contracts\OptionPluginInterface
   *   The option plugin instance.
   */
  protected function getOptionPlugin(string $option_id) {
    $option_definition = $this
      ->optionPluginConfigurationDiscovery()
      ->getOptionDefinition($option_id);
    $instance = $this->optionPluginManager()->createInstance(
      $option_definition['plugin'],
      $option_definition
    );
    return $instance;
  }

  /**
   * Returns the option plugin configuration discovery service.
   *
   * @return \Drupal\option_plugin\OptionPluginConfigurationDiscovery
   *   The option plugin configuration discovery service.
   */
  protected function optionPluginConfigurationDiscovery() {
    return \Drupal::service('option_plugin.discovery');
  }

  /**
   * Returns the option attribute plugin manager service.
   *
   * @return \Drupal\option_plugin\Contracts\OptionPluginManagerInterface
   *   The option plugin configuration discovery service.
   */
  protected function optionPluginManager() {
    return \Drupal::service('plugin.manager.option_plugin');
  }

}
