<?php

declare(strict_types=1);

namespace Drupal\option_plugin\Plugin\OptionPlugin;

use Drupal\Core\Form\FormStateInterface;
use Drupal\option_plugin\OptionPluginStyleTrait;
use Drupal\option_plugin\Plugin\OptionPluginBase;

/**
 * Define the image attribute option plugin.
 *
 * @OptionPlugin(
 *   id = "background_color",
 *   label = @Translation("Background Color"),
 * )
 */
class BackgroundColor extends OptionPluginBase {

  use OptionPluginStyleTrait;

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['color'] = [
      '#type' => 'color_spectrum',
      '#title' => $this->getLabel(),
      '#default_value' => $this->getValue('color') ?? $this->getDefaultValue(),
      '#settings' => $this->getConfiguration('settings'),
      '#wrapper_attributes' => [
        'class' => [$this->getConfiguration('css_class')],
      ],
      '#description' => $this->getConfiguration('description'),
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function build(array $build) {
    $value = $this->getValue('color') ?? NULL;
    if (!empty($value)) {
      if ($this->getConfiguration('method') == 'css') {
        $this->generateStyle($build, ['#color' => $value]);
      }
      else {
        $build['#attributes']['style'][] = "background-color: $value;";
      }
    }
    return $build;
  }

}
