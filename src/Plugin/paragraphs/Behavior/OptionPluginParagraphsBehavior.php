<?php

namespace Drupal\option_plugin\Plugin\paragraphs\Behavior;

use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs\ParagraphsBehaviorBase;
use Drupal\option_plugin\OptionPluginContextTrait;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;

/**
 * Provides a way to define grid based layouts.
 *
 * @ParagraphsBehavior(
 *   id = "option_plugin",
 *   label = @Translation("Option Plugin"),
 *   description = @Translation("Integrates paragraphs with option plugins."),
 *   weight = 0
 * )
 */
class OptionPluginParagraphsBehavior extends ParagraphsBehaviorBase {

  use OptionPluginContextTrait;

  /**
   * {@inheritDoc}
   */
  public function buildBehaviorForm(
    ParagraphInterface $paragraph,
    array &$form,
    FormStateInterface $form_state
  ) {

    $context_options = $this
      ->optionPluginConfigurationDiscovery()
      ->getContextOptions($this->getContextId(), $paragraph->bundle());

    foreach (array_keys($context_options) as $option_id) {
      $values = $paragraph->getBehaviorSetting($this->pluginId, $option_id);
      $form[$option_id] = $this->getOptionPluginForm($option_id, $form, $form_state, $values);
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {

    $context_options = $this
      ->optionPluginConfigurationDiscovery()
      ->getContextOptions($this->getContextId(), $paragraph->bundle());

    foreach (array_keys($context_options) as $option_id) {
      $values = $paragraph->getBehaviorSetting($this->pluginId, $option_id);
      $build = $this->buildOptions($option_id, $values, $build);
    }
  }

  /**
   * Returns the context id.
   *
   * @return string
   *   The context id.
   */
  public static function getContextId() {
    return 'paragraphs';
  }

}
