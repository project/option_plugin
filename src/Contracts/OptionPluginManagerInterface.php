<?php

declare(strict_types=1);

namespace Drupal\option_plugin\Contracts;

use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Define the attribute option manager interface.
 */
interface OptionPluginManagerInterface extends PluginManagerInterface {

}
