<?php

declare(strict_types=1);

namespace Drupal\option_plugin\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Define the attribute option plugin.
 *
 * @Annotation
 */
class OptionPlugin extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The plugin label.
   *
   * @var string
   */
  public $label;

}
